1724757390


 it turns out you can play games straight from your calculator – including classic titles like Grand Theft Auto (GTA), Doom and Skyrim.

Toying around with his TI-Nspire CX graphing device, Redditor RogueConditional has managed to run GTA Advance (the 2004 version of the game for Nintendo’s Game Boy Advance) on the calculator entirely natively.

